﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BicycleRentApp.Models.Authentication;
using BLL.DTO.Authentication;
using BLL.Interfaces.Authentication;
using DAL.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BicycleRentApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private ILoginService loginService;

        private ISignUpService signUpService;

        public AuthController(ILoginService loginService, ISignUpService signUpService)
        {
            this.loginService = loginService;
            this.signUpService = signUpService;
        }

        [HttpPost, Route("login")]
        public IActionResult Login([FromBody]LoginModel model)
        {
            if (model == null)
            {
                return BadRequest("Invalid client request");
            }

            loginService.SetModel(new LoginDTO() { Email = model.Email, Password = model.Password });

            var response = loginService.Login();

            if (!response.IsSuccessful)
            {
                return Unauthorized(new { message = response.ErrorMessage });
            }
            else
            {
                return Ok(new { Token = response.ResponseText });
            }
        }

        [HttpPost, Route("signUp")]
        public IActionResult SignUp([FromBody]SignUpModel model)
        {
            if (model == null)
            {
                return BadRequest("Invalid client request");
            }

            signUpService.SetModel(new SignUpDTO()
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                Password = model.Password,
                ConfirmPassword = model.ConfirmPassword,
                City = model.City,
                PhoneNumber = model.PhoneNumber,
                Male = model.Male == "0" ? Males.Male : Males.Female
            }) ;

            var response = signUpService.SignUp();

            if (!response.IsSuccessful)
            {
                return BadRequest(new { message = response.ErrorMessage });
            }
            else
            {
                return Ok(new { Token = response.ResponseText });
            }
        }
    }
}