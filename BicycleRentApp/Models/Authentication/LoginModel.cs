﻿using System.ComponentModel.DataAnnotations;

namespace BicycleRentApp.Models.Authentication
{
    public class LoginModel
    {
        [Required]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}", ErrorMessage = "Wrong format!")]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
