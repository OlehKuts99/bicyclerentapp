﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BicycleRentApp.Models.Authentication
{
    public class SignUpModel
    {
        [Required]
        [RegularExpression(@"^\D{1,30}$")]
        public string FirstName { get; set; }

        [Required]
        [RegularExpression(@"^\D{1,30}$")]
        public string LastName { get; set; }

        [Required]
        [RegularExpression(@"^\D{1,30}$")]
        public string City { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [Phone]
        public string PhoneNumber { get; set; }

        public string Male { get; set; }

        [Required]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}", ErrorMessage = "Wrong format!")]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}
