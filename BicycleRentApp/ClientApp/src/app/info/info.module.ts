import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material/material.module'
import { Routes } from '@angular/router';

import { InfoComponent } from './info.component'
import { NewsComponent } from './news/news.component'
import { SalesComponent } from './sales/sales.component'
import { AboutUsComponent } from './about-us/about-us.component'
import { SupportComponent } from './support/support.component';


const infoRoute : Routes = [
  { path: '', component: InfoComponent ,
    children: [
      { path: 'news', component: NewsComponent },
      { path: 'aboutus', component: AboutUsComponent },
      { path: 'sales', component: SalesComponent },
      { path: 'support', component: SupportComponent },

    ]
  },
  { path: "**", redirectTo: ''}
]

@NgModule({
  declarations: [
    InfoComponent,
    NewsComponent,
    AboutUsComponent,
    SupportComponent,
    SalesComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(infoRoute),
    MaterialModule
  ]
})
export class InfoModule { }
