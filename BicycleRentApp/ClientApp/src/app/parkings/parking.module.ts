import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material/material.module'
import { Routes } from '@angular/router';

import { ParkingItemComponent } from './parking-item/parking-item.component'
import { ParkingListComponent } from './parking-list/parking-list.component'
import { ParkingsComponent } from './parkings.component';

const parkingRoute : Routes = [
  { path: '', component: ParkingListComponent ,},
  { path: ':id', component: ParkingItemComponent ,},
]

@NgModule({
  declarations: [
    ParkingsComponent,
    ParkingListComponent,
    ParkingItemComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(parkingRoute),
    MaterialModule
  ]
})
export class ParkingModule { }
