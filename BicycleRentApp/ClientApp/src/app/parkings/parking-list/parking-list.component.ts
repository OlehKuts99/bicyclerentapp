import { Component, OnInit } from '@angular/core';

import { Mock } from '../mock'
import { from } from 'rxjs';

@Component({
  selector: 'parking-list',
  templateUrl: './parking-list.component.html',
  styleUrls: ['./parking-list.component.css']
})
export class ParkingListComponent implements OnInit {
  mocks: Mock[];
  constructor() { 
    this.mocks = [
      { id: 11, name: 'Dr Nice', adress: 'Rubchaka, 6', bicycleCount: 1, containsTerminal: true, inBuilding: false, schedule: '10-18' },
      { id: 12, name: 'Dr Lol', adress: 'Upa, 6', bicycleCount: 13, containsTerminal: true, inBuilding: false, schedule: '10-18' },
      { id: 13, name: 'Dr Kek', adress: 'Shevchenka, 111', bicycleCount: 4, containsTerminal: true, inBuilding: true, schedule: '10-18' },
      { id: 14, name: 'Dr Test', adress: 'Oleny, 45', bicycleCount: 7, containsTerminal: false, inBuilding: false, schedule: '10-18' },]
  }
  
  ngOnInit() {
  }

}
