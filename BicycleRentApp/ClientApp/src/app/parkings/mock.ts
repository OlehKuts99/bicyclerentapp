export class Mock {
    id: number;
    name: string;
    adress: string;
    bicycleCount: number;
    containsTerminal: boolean;
    inBuilding: boolean;
    schedule: string;
  }
  