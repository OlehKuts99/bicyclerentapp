import { Component, OnInit } from '@angular/core';

import { Mock } from '../mock';

@Component({
  selector: 'app-parking-item',
  templateUrl: './parking-item.component.html',
  styleUrls: ['./parking-item.component.css']
})
export class ParkingItemComponent implements OnInit {

  parking: Mock;
  constructor() { 
    this.parking = 
    { id: 11, name: 'Dr Nice', adress: 'Rubchaka, 6', bicycleCount: 1, containsTerminal: true, inBuilding: false, schedule: '10-18' }
  }

  ngOnInit() {
  }

}
