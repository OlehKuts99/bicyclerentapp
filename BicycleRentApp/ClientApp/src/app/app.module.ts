import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SharedModule } from './common/shared.module'
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { MaterialModule } from './material/material.module'

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component'
import { NavMenuComponent } from './nav-menu/nav-menu.component'
import { FooterComponent } from './footer/footer.component'

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { from } from 'rxjs'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    NavMenuComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    SharedModule,
    MaterialModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      {
        path: 'authentication',
        loadChildren: 'src/app/authentication/authentication.module#AuthenticationModule'
      },
      {
        path: 'info',
        loadChildren: 'src/app/info/info.module#InfoModule'
      },
      {
        path: 'parkings',
        loadChildren: 'src/app/parkings/parking.module#ParkingModule'
      }
    ]),
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
