import { NgModule } from '@angular/core'
import { SharedModule } from '../common/shared.module'
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material/material.module'
import { Routes } from '@angular/router';

import { AuthenticationComponent } from './authentication.component'
import { LoginComponent } from './login/login.component'
import { RegistrationComponent } from './registration/registration.component'
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component'

import { LoginService } from './services/login.service'
import { SignUpService } from './services/sign-up.service'
import { AuthInterceptor } from './services/auth.interceptor'
import { HTTP_INTERCEPTORS } from '@angular/common/http';

const authenticationRoute : Routes = [
  { path: '', component: AuthenticationComponent ,
    children: [
      { path: '', component: LoginComponent },
      { path: 'registration', component: RegistrationComponent },
      { path: 'forgot', component: ForgotPasswordComponent },
    ]
  },
  { path: "**", redirectTo: ''}
]

@NgModule({
  declarations: [
    AuthenticationComponent,
    LoginComponent,
    ForgotPasswordComponent,
    RegistrationComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(authenticationRoute),
    MaterialModule,
  ],
  providers: [LoginService, SignUpService, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }]
})
export class AuthenticationModule { }
