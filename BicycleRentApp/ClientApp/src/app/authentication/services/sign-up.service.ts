import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class SignUpService {

    private path: string = "https://localhost:44311/api"

    constructor(private http: HttpClient, private toastrService: ToastrService,
        private router: Router) { }

    signUp(form: FormGroup) {
        console.log(form)

        this.http.post<any>(this.path + "/auth/signUp", form.value).subscribe(
            res => {
                localStorage.setItem('token', res.token); this.router.navigate(['/']);
                this.toastrService.success("You are signed in!")},
            error => {
                this.handleError(error)
            })
    }

    handleError(error: HttpErrorResponse) {
        error.status === 400 ?
            this.toastrService.error(error.error.message)
            : console.log(error.message)
    }
}
