import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root',
})
export class LoginService {

    private path: string = "https://localhost:44311/api"

    constructor(private http: HttpClient, private toastrService: ToastrService,
        private router: Router) { }

    get isAuthenticated() {
        return !!localStorage.getItem('token')
    }

    login(form: FormGroup) {
        this.http.post<any>(this.path + "/auth/login", form.value).subscribe(
            res => {
                localStorage.setItem('token', res.token); this.router.navigate(['/']);
                this.toastrService.success("You are logged in!")
            },
            error => { this.handleError(error) })
    }

    logOut() {
        localStorage.removeItem('token');
        this.toastrService.success("You are logged out!")
    }

    handleError(error: HttpErrorResponse) {
        error.status === 401 ? this.toastrService.error(error.error.message)
            : error.status === 400 ? this.toastrService.error(error.error.message)
                : console.log(error.message)
    }
}
