﻿using DAL.Context;
using DAL.Intrefaces.Repositories;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Classes.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private ApplicationContext applicationContext;

        public UserRepository(ApplicationContext context)
        {
            applicationContext = context;
        }

        public async void Create(User user)
        {
            await applicationContext.Users.AddAsync(user);
        }

        public async void Delete(int id)
        {
            var tempUser = await applicationContext.Users.FindAsync(id);

            if (tempUser != null)
            {
                applicationContext.Users.Remove(tempUser);
            }
        }

        public IEnumerable<User> Find(Func<User, bool> predicate)
        {
            return applicationContext.Users.Where(predicate).ToList();
        }

        public async Task<User> Get(int id)
        {
            return await applicationContext.Users.FindAsync(id);
        }

        public IEnumerable<User> GetAll()
        {
            return applicationContext.Users.ToList();
        }

        public void Update(User user)
        {
            applicationContext.Entry(user).State = EntityState.Modified;
        }
    }
}
