﻿using DAL.Classes.Repositories;
using DAL.Context;
using DAL.Intrefaces;
using DAL.Intrefaces.Repositories;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Classes
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationContext applicationContext;

        public UnitOfWork(ApplicationContext context)
        {
            applicationContext = context;
        }

        public IRepository<User> Users 
        {
            get
            {
                return new UserRepository(applicationContext);
            }
        }

        public async void SaveChanges()
        {
            await applicationContext.SaveChangesAsync();
        }
    }
}
