﻿using DAL.Intrefaces.Repositories;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Intrefaces
{
    public interface IUnitOfWork
    {
        IRepository<User> Users { get; }

        void SaveChanges();
    }
}
