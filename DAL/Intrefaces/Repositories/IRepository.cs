﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Intrefaces.Repositories
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();

        Task<T> Get(int id);

        IEnumerable<T> Find(Func<T, Boolean> predicate);

        void Create(T item);

        void Update(T item);

        void Delete(int id);
    }
}
