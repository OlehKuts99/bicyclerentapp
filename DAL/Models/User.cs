﻿using DAL.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class User
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string City { get; set; }

        public string PhoneNumber { get; set; }

        public Males Male { get; set; }

        public string Email { get; set; }

        public byte[] Password { get; set; }

        public DateTime DateOfBirth { get; set; }
    }
}
