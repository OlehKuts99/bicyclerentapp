﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BLL.BussinesModels
{
    public class SettingsHandler
    {
        public dynamic GetTokenSection()
        {
            var settings = LoadSettings();
            var obj = JsonConvert.DeserializeObject<dynamic>(settings);

            return obj.TokenSettings;
        }

        private string LoadSettings()
        {
            var result = new StringBuilder();

            using (var reader = new StreamReader("../BLL/Settings/settings.json"))
            { 
                while(!reader.EndOfStream)
                {
                    result.AppendLine(reader.ReadLine());
                }
            }

            return result.ToString();
        }
    }
}
