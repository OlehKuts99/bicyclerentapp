﻿using BLL.DTO;
using BLL.DTO.Authentication;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Interfaces.Authentication
{
    public interface ILoginService
    {
        void SetModel(LoginDTO model);

        HttpResponse Login();
    }
}
