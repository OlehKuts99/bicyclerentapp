﻿using BLL.DTO;
using BLL.DTO.Authentication;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Interfaces.Authentication
{
    public interface ISignUpService
    {
        void SetModel(SignUpDTO model);

        HttpResponse SignUp();
    }
}
