﻿using DAL.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO.Authentication
{
    public class SignUpDTO
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string City { get; set; }

        public string PhoneNumber { get; set; }

        public Males Male { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }
}
