﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class HttpResponse
    {
        public bool IsSuccessful { get; set; }

        public string ResponseText { get; set; }

        public string ErrorMessage { get; set; }
    }
}
