﻿using BLL.DTO;
using BLL.DTO.Authentication;
using BLL.Interfaces.Authentication;
using DAL.Intrefaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace BLL.Services.Authentication
{
    public class SignUpService : ISignUpService
    {
        private IUnitOfWork unitOfWork;

        private ILoginService loginService;

        private SignUpDTO signUpModel;

        public SignUpService(IUnitOfWork unitOfWork, ILoginService loginService)
        {
            this.unitOfWork = unitOfWork;
            this.loginService = loginService;
        }

        public HttpResponse SignUp()
        {
            if (!IsRegistered(signUpModel.Email))
            {
                if (AddUser())
                {
                    loginService.SetModel(new LoginDTO() { Email = signUpModel.Email, Password = signUpModel.Password });

                    return loginService.Login();
                }
                else
                {
                    return new HttpResponse() { IsSuccessful = false, ErrorMessage = "Registration error!" };
                }
            }
            else
            {
                loginService.SetModel(new LoginDTO() { Email = signUpModel.Email, Password = signUpModel.Password });

                return loginService.Login();
            }
        }

        public void SetModel(SignUpDTO model)
        {
            signUpModel = model;
        }

        private bool AddUser()
        {
            var user = CreateUser(signUpModel);

            try
            {
                unitOfWork.Users.Create(user);
                unitOfWork.SaveChanges();
            }
            catch(Exception)
            {
                return false;
            }

            return true;
        }

        private User CreateUser(SignUpDTO model)
        {
            var user = new User()
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                Password =  ProtectedData.Protect(Encoding.UTF8.GetBytes(model.Password), 
                    null, DataProtectionScope.CurrentUser),
                City = model.City,
                PhoneNumber = model.PhoneNumber,
                Male = model.Male
            };

            return user;
        }

        private bool IsRegistered(string email)
        {
            var user = unitOfWork.Users.Find(u => u.Email == email).FirstOrDefault();

            return user != null ? true : false;
        }
    }
}
