﻿using BLL.BussinesModels;
using BLL.DTO;
using BLL.DTO.Authentication;
using BLL.Interfaces.Authentication;
using DAL.Intrefaces;
using DAL.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace BLL.Services.Authentication
{
    public class LoginService : ILoginService
    {
        private LoginDTO loginModel;

        private IUnitOfWork unitOfWork;

        public LoginService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public HttpResponse Login()
        {
            var user = IsRegistered();

            if (user != null)
            {
                if (CheckPassword(user.Id, loginModel.Password))
                {
                    var tokenHandler = new JwtSecurityTokenHandler();

                    var token = tokenHandler.CreateToken(ConfigureToken(user));
                    var tokenString = tokenHandler.WriteToken(token);

                    return new HttpResponse() { IsSuccessful = true, ResponseText = tokenString };
                }

                return new HttpResponse() { IsSuccessful = false, ErrorMessage = "Password is not valid!" };
            }

            return new HttpResponse() { IsSuccessful = false, ErrorMessage = "Account doesn't exist!" };
        }

        public void SetModel(LoginDTO model)
        {
            loginModel = model;
        }

        private User IsRegistered()
        {
            var user = unitOfWork.Users.Find(u => u.Email == loginModel.Email).FirstOrDefault();

            return user ?? null;
        }

        private bool CheckPassword(int userId, string password)
        {
            var userPassword = unitOfWork.Users.Get(userId).Result.Password;

            var bytes = ProtectedData.Unprotect(userPassword, null, DataProtectionScope.CurrentUser);
            var decryptedPassword = Encoding.UTF8.GetString(bytes);

            if (decryptedPassword == password)
            {
                return true;
            }

            return false;
        }

        private SecurityTokenDescriptor ConfigureToken(User user)
        {
            var settings = new SettingsHandler();
            var tokenSettings = settings.GetTokenSection();

            var key = Encoding.UTF8.GetBytes(tokenSettings.secretKey.ToString());

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Email, user.Email),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                }),
                Expires = DateTime.UtcNow.AddMinutes(double.Parse(tokenSettings.minutes.ToString())),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256),
                Audience = tokenSettings.audience.ToString(),
                Issuer = tokenSettings.issuer.ToString()
            };

            return tokenDescriptor;
        }
    }
}
